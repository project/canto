/**
 * @file
 * Contains js for the canto connector.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.cantoEntityBrowser = {
    attach: function (context, settings) {
      $(function () {
        console.log("allowExtension:" + drupalSettings.canto.allowExtensions);
        $('#cantoUC').cantoUC({
          env: drupalSettings.canto.env ? drupalSettings.canto.env : 'canto.com',
          accessToken: drupalSettings.canto.accessToken,
          tenants: drupalSettings.canto.tenants,
          tokenType: drupalSettings.canto.tokenType,
          extensions: drupalSettings.canto.allowExtensions
        }, Drupal.behaviors.cantoEntityBrowser.cantoFilePopupCallback);
      });
    },
    cantoFilePopupCallback:  (id, assetArray) => {
      const val = encodeURIComponent(JSON.stringify(assetArray))
      console.log(val);
      // @todo fix hard class.
      $('.entity-browser-form').append('<div class="ajax-progress-fullscreen"></div>');
      document.getElementById("canto-assets-data").value = val;
      // Trigger form submission
      document.getElementsByClassName('is-entity-browser-submit button button--primary js-form-submit form-submit')[0].click();
    }
  };
})(jQuery, Drupal, drupalSettings);
