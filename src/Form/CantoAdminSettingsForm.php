<?php

namespace Drupal\canto\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin form for canto module.
 */
class CantoAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'canto_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'canto.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('canto.settings');

    $form['env'] = [
      '#type' => 'select',
      '#title' => $this->t('Canto Environment selection'),
      '#options' => [
        'canto.com' => $this->t('canto.com'),
        'canto.global' => $this->t('canto.global'),
        'canto.de' => $this->t('canto.de'),
        'ca.canto.com' => $this->t('ca.canto.com'),
        'au.canto.com' => $this->t('au.canto.com'),
      ],
      '#default_value' => $config->get('env') ?? 'canto.com',
      '#attributes' => [
        'data-editor-canto-canto' => 'env',
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('canto.settings')
      ->set('env', $form_state->getValue('env'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
